# BSA Academy - 2020 #

### Задача: React (part 1) ###

Нужно написать приложение Чат. User Interface чата должен содержать следующие блоки:
* header: отдельный компонент, который содержит название чата, количество пользователей, количество сообщений, дата последнего сообщения

* message list: отдельный компонент который содержит список сообщений. Список должен быть разделен по дням (линия с указанным днем, типа "Yesterday", Monday, 17 June ", etc.)

* message input: отдельный компонент, который содержит текстовое поле и кнопку "Send"

### Требования к заданию: ###

* весь User Interface должен быть построен на компонентах и ​​находиться в главном компоненте-контейнере Chat

* данные загружаются в Chat со стороннего ресурса и дальше хранятся в state этого компонента

* при загрузке страницы появляется Спиннер, и исчезает после того, как данные загрузились

* чат должен иметь следующий функционал:

    * писать сообщения в чат

    * редактировать сообщение (личное)

    * удалять сообщение (личное)

    * ставить лайк (не личное)

* каждое сообщение должно содержать следующие блоки:

    * аватар (кроме собственного сообщения)

    * текст сообщения

    * дата сообщения

    * лайк

* Использовать только библиотеку React (без backend, без Redux и других библиотек)
