import React from 'react';
import './chat-header.css';

function ChatHeader() {
  return (
    <header className="chat-header">
      <h3 className="chat-header__title">My chat</h3>
      <div className="chat-header__participants">
        <span><b>23</b></span> participant(s)
      </div>
      <div className="chat-header__msg-count">
        <span><b>53</b></span> message(s)
      </div>
      <div className="chat-header__last-msg">
        last message at <span><b>14.28</b></span>
      </div>
    </header>
  );
}

export default ChatHeader;
