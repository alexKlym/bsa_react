import React from 'react';
import logo from '../../assets/logo.svg';
import './app.css';
import Chat from "../chat/chat";

function App() {
  return (
    <div className="app">
      <header className="app-header container">
        <div className="app-header__logo">
          <img src={logo} className="app-header__logo-img" alt="logo"/>
        </div>
        <h1 className="app-header__title">React chat</h1>
      </header>

      <main className="app-content container">
        <Chat/>
      </main>

      <footer className="app-footer container">
        <p className="app-footer__text">&copy; BSA Team</p>
      </footer>
    </div>
  );
}

export default App;
