import React, {useState} from 'react';
import './message-input.css';

function MessageInput() {
  const [value, setValue] = useState('');

  function submitHandler(event) {
    event.preventDefault();
    const message = value.trim();

    if (message) {
      sendMessage(message);
    }
  }
  function sendMessage(message) {
    console.log('Send: ', message);
    setValue('');
  }

  return (
    <form className="message-input" onSubmit={submitHandler}>
      <input type="text"
             value={value}
             onChange={event => setValue(event.target.value)}
             placeholder="Message"
             className="message-input__field"/>
      <button type="submit"
              className="message-input__btn">Send</button>
    </form>
  );
}

export default MessageInput;
