import React from 'react';
import './chat.css';
import ChatHeader from '../chat-header/chat-header';
import MessageList from '../message-list/message-list';
import MessageInput from "../message-input/message-input";

function Chat() {

  (function fetchMessages() {
    fetch('https://api.npoint.io/b919cb46edac4c74d0a8').then((data) => {
      const result = data.json();
      result.then((value) => {
        console.log('Messages history: ', value);
      })
    })
  })()

  return (
    <div className="chat-wrapper">
      <ChatHeader/>
      <div className="chat-body">
        <MessageList/>
      </div>
      <MessageInput/>
    </div>
  );
}

export default Chat;
