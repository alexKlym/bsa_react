import React from 'react';
import './message-list.css';
// import PropTypes from 'prop-types';

function MessageList() {
  return (
    <div>
      <ul className="message-list">
        <li className="message">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque, odit, veniam. Deserunt enim in incidunt rem reprehenderit. Earum, obcaecati, vitae?</li>
        <li className="message">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>

        <li className="message message--right">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias consequatur dignissimos earum fugiat ipsum modi nesciunt omnis repellendus voluptatum. Accusantium, aliquid dolor et facere in numquam repellendus voluptatibus. Assumenda consectetur cumque nihil? Ad dolor esse et incidunt nostrum odit quas.</li>
      </ul>
      <div className="message-divider">
        <span/>
        <p className="message-divider__date">Yesterday</p>
        <span/>
      </div>
      <ul className="message-list">
        <li className="message">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque, odit, veniam. Deserunt enim in incidunt rem reprehenderit. Earum, obcaecati, vitae?</li>
        <li className="message">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>

        <li className="message message--right">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias consequatur dignissimos earum fugiat ipsum modi nesciunt omnis repellendus voluptatum. Accusantium, aliquid dolor et facere in numquam repellendus voluptatibus. Assumenda consectetur cumque nihil? Ad dolor esse et incidunt nostrum odit quas.</li>
      </ul>
    </div>
  );
}

// MessageList.propTypes = {
//   messages: PropTypes.arrayOf(PropTypes.object).isRequired
// }

export default MessageList;
