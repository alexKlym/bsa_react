import React from 'react';
import PropTypes from 'prop-types';

function Message() {
  return (
    <ul>
      <li>Msg #1</li>
      <li>Msg #2</li>
      <li>Msg #3</li>
    </ul>
  );
}

Message.propTypes = {
  message: PropTypes.exact({
    avatar: PropTypes.string,
    createdAt: PropTypes.string,
    editedAt: PropTypes.string,
    id: PropTypes.string,
    text: PropTypes.string,
    user: PropTypes.string,
    userId: PropTypes.string,
  }).isRequired
}

export default Message;
